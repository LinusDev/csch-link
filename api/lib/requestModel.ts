var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var requestSchema = new Schema({
  ip: String,
  lastRequest: Date,
  tries: Number
})

module.exports = mongoose.model('request', requestSchema)   