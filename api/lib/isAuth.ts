import * as jwt from 'jsonwebtoken'

const getTokenFromHeader = (req) => {
  //extract cookie from request
  if (req.headers.cookie && req.headers.cookie.split('=')[0] === 'jwt') {
    //get token from cookie
    return req.headers.cookie.split('=')[1];
  }
}

module.exports = async (req) => {
  const token = getTokenFromHeader(req)
  if (!token) return false
  const { err } = await jwt.verify(token, process.env.JWT_SECRET)
  if (err) {
    console.error('jwterr', err)
    return false
  }
  const data = await jwt.decode(token)
  console.log('got auth data', data)
  return data;
}
