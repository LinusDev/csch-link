var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: String,
  password: String,
  salt: String,
  role: String
})

module.exports = mongoose.model('users', userSchema)   