import * as argon2 from 'argon2'
const UserModel = require('./userModel')
const generateJWT = require('./generateToken')
const connectdb = require('./db')

module.exports = async (username, password): Promise<any> => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Database Connection
  await connectdb()
  console.log('searching user')
  const userRecord = await UserModel.findOne({ username: username })
  if (!userRecord) {
    throw new Error('User not found')
  } else {
    console.log('found user')
    const correctPassword = await argon2.verify(userRecord.password, password)
    if (!correctPassword) {
      throw new Error('Incorrect password')
    }
  }

  return {
    user: {
      username: userRecord.username,
      role: userRecord.role
    },
    token: generateJWT(userRecord),
  }
}
