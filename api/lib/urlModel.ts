var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var urlSchema = new Schema({
  alias: String,
  url: String
})

module.exports = mongoose.model('url', urlSchema)   