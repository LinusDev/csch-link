import * as argon2 from 'argon2'
const UserModel = require('./userModel')
import { randomBytes } from 'crypto'
const connectdb = require('./db')

module.exports = async (username, password): Promise<any> => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Database Connection
  await connectdb()

  const salt = randomBytes(32)
  const passwordHashed = await argon2.hash(password, { salt })

  const userRecord = await new UserModel({
    username,
    password: passwordHashed,
    salt: salt.toString('hex'),
    role: 'user'
  }).save()
  return {
    user: {
      username: userRecord.username,
      role: userRecord.role
    },
  }
}