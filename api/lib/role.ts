import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const isAuth = require('./isAuth')

module.exports = async (req: IncomingMessage, role: String) => {
  const user = await isAuth(req)
  return user.role
}