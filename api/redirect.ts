import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const { parse } = require("url")
const connectdb = require('./lib/db')
const UrlModel = require('./lib/urlModel')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Database Connection
  await connectdb()

  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const { query } = parse(req.url, true)
  const { alias } = query

  //
  // ─── REDIRECT ──────────────────────────────────────────────────────────────
  //

  UrlModel.findOne({ alias: alias }, function (err, obj) {
    if (err) console.error(err)
    if (!obj) {
      send(res, 404, { Error: { code: 1, message: `Can't find redirect url` } })
      return
    }
    const redirect = obj.url

    // Redirect user
    res.setHeader('Location', redirect)
    send(res, 302, { message: `Redirecting to ${alias}` })
  })

}