import { IncomingMessage, ServerResponse } from 'http'
import { send, json } from 'micro'
const SignUp = require('./lib/signUp')
const rateLimit = require('./lib/rateLimit')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  // Rate Limit
  const limit = await rateLimit(req, 60000, 1)
  if (!limit) {
    send(res, 429, { Error: { code: 7, message: `Too many requests`, value: limit } })
    return
  }

  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const reqBody = await json(req)
  const { username, password } = reqBody.user;

  //
  // ─── SIGNUP ──────────────────────────────────────────────────────────────
  //

  if(username.length < 3) {
    return send(res, 400, 'Username has to be at least 3 characters long')
  } else if(password.length < 6) {
    return send(res, 400, 'Password has to be at least 5 characters long')
  }

  try {
    const { user } = await SignUp(username, password);
    return send(res, 200, user)
  } catch (error) {
    return send(res, 500, error.message)
  }
}