import { IncomingMessage, ServerResponse } from 'http'
import { send, json } from 'micro'
const SignIn = require('./lib/signIn')
const rateLimit = require('./lib/rateLimit')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  // Rate Limit
  const limit = await rateLimit(req, 5000, 3)
  if (!limit) {
    send(res, 429, { Error: { code: 7, message: `Too many requests`, value: limit } })
    return
  }


  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const reqBody = await json(req)
  const { username, password } = reqBody.user;


  //
  // ─── SIGNIN ──────────────────────────────────────────────────────────────
  //

  try {
    const response = await SignIn(username, password)
    return send(res, 200, response)
  } catch (error) {
    return send(res, 500, {error: error.message})
  }
}
